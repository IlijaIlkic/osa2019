INSERT INTO osa_proba.users (user_firstname, user_lastname, user_password, user_username) VALUES ('admin', 'admin', 'admin', 'admin');
INSERT INTO osa_proba.accounts ( account_displayname, account_in_server_address, account_in_server_port, account_pop3_imap, account_in_server_type, account_password,account_smtp_adress, account_smtp_port, account_username, user_id)VALUES('admin', 'ddd', 2230,'pop3' ,123, 'zeljko123', 'admin', 2233, 'zeljko.brkic98@gmail.com', 1);
INSERT INTO osa_proba.folders(folder_name, account_id, parent_folder_id)VALUES('Inbox', 1, null);
INSERT INTO osa_proba.folders(folder_name, account_id, parent_folder_id)VALUES('Drafts', 1, null);
INSERT INTO osa_proba.folders(folder_name, account_id, parent_folder_id)VALUES('Sent', 1, null);
INSERT INTO osa_proba.folders(folder_name, account_id, parent_folder_id)VALUES('prvi_folder', 1, null);
INSERT INTO osa_proba.folders(folder_name, account_id, parent_folder_id)VALUES('drugi_folder', 1, 4);
INSERT INTO osa_proba.messages(message_bcc, message_cc, message_content,message_date_time, message_from,message_subject, message_to, message_unread, account_id, folder_id)VALUES(null,null,'dasasddas das sdasasdas asdsaddsa','2018-03-02 21:10:03','pera@gmail.com','Naslov','djoka@gmail.com',1,1,1);
INSERT INTO osa_proba.messages(message_bcc, message_cc, message_content,message_date_time, message_from,message_subject, message_to, message_unread, account_id, folder_id)VALUES(null,null,'Tekst je doar','2019-03-02 21:11:03','steva@gmail.com','Novi naslov','luka@gmail.com',0,1,1);
INSERT INTO osa_proba.messages(message_bcc, message_cc, message_content,message_date_time, message_from,message_subject, message_to, message_unread, account_id, folder_id)VALUES(null,null,'Lorem ipsum dolor ','2017-03-02 21:10:03','lok@gmail.com','Onda jedan','pok@gmail.com',0,1,1);
INSERT INTO osa_proba.messages(message_bcc, message_cc, message_content,message_date_time, message_from,message_subject, message_to, message_unread, account_id, folder_id)VALUES(null,null,'Tekst je los','2016-04-03 20:10:03','thor@gmail.com','Onda broj','david@gmail.com',0,1,1);
INSERT INTO osa_proba.messages(message_bcc, message_cc, message_content,message_date_time, message_from,message_subject, message_to, message_unread, account_id, folder_id)VALUES(null,null,'Tekst je los','2015-04-03 20:10:03','ledda@gmail.com','Djoka','pera@gmail.com',0,1,1);
INSERT INTO osa_proba.rules(rule_condition, rule_operation, rule_value, folder_id)VALUES(2, 1, 'vrednost', 1);
INSERT INTO osa_proba.rules(rule_condition, rule_operation, rule_value, folder_id)VALUES(1, 2, 'vrednost2', 2);
INSERT INTO osa_proba.rules(rule_condition, rule_operation, rule_value, folder_id)VALUES(1, 2, 'vrednost3', 3);
INSERT INTO osa_proba.rules(rule_condition, rule_operation, rule_value, folder_id)VALUES(3, 1, 'vrednost3', 4);
INSERT INTO osa_proba.rules(rule_condition, rule_operation, rule_value, folder_id)VALUES(0, 2, 'vrednost3', 5);
INSERT INTO osa_proba.tags(tag_name, user_id)VALUES('perica', 1);
INSERT INTO osa_proba.contacts(contact_displayname, contact_email, contact_format, contact_firstname,contact_lastname, contact_note, user_id)VALUES('jovan', 'jovan@gmail.com',1 ,'perica', 'jovic','dobar', 1);
INSERT INTO osa_proba.attachments(attachment_data, attachment_mime_type, attachment_name, message_id)VALUES(null,'bravo', 'zdravo', 1);
INSERT INTO osa_proba.photos(photo_path,contact_id)VALUES('https://previews.123rf.com/images/tuktukdesign/tuktukdesign1606/tuktukdesign160600105/59070189-user-icon-man-profile-businessman-avatar-person-icon-in-vector-illustration.jpg',1);
INSERT INTO osa_proba.message_tags(message_id, tag_id)VALUES(1, 1);
--da
INSERT INTO osa_proba.users (user_firstname, user_lastname, user_password, user_username) VALUES ('user', 'user', 'user', 'user');
INSERT INTO osa_proba.accounts ( account_displayname, account_in_server_address, account_in_server_port, account_pop3_imap, account_in_server_type, account_password,account_smtp_adress, account_smtp_port, account_username, user_id)VALUES('user', 'ddd', 2230,'pop3' ,1234, 'user', 'user', 22334, 'user', 1);
INSERT INTO osa_proba.folders(folder_name, account_id, parent_folder_id)VALUES('Inbox', 2, null);
INSERT INTO osa_proba.folders(folder_name, account_id, parent_folder_id)VALUES('Drafts', 2, null);
INSERT INTO osa_proba.folders(folder_name, account_id, parent_folder_id)VALUES('Sent', 2, null);
INSERT INTO osa_proba.folders(folder_name, account_id, parent_folder_id)VALUES('prvi_folder', 2, null);
INSERT INTO osa_proba.folders(folder_name, account_id, parent_folder_id)VALUES('drugi_folder', 2, 9);
INSERT INTO osa_proba.messages(message_bcc, message_cc, message_content,message_date_time, message_from,message_subject, message_to, message_unread, account_id, folder_id)VALUES(null,null,'fdsdsfdas das sdasasdas asdsaddsa','2018-03-02 21:10:03','pera@gmail.com','Naslov','djoka@gmail.com',1,2,1);
INSERT INTO osa_proba.messages(message_bcc, message_cc, message_content,message_date_time, message_from,message_subject, message_to, message_unread, account_id, folder_id)VALUES(null,null,'Teksfdsfdt je doar','2019-06-02 21:11:03','steva@gmail.com','Novi naslov','luka@gmail.com',0,2,1);
INSERT INTO osa_proba.messages(message_bcc, message_cc, message_content,message_date_time, message_from,message_subject, message_to, message_unread, account_id, folder_id)VALUES(null,null,'Lorfsdfdsem ipsum dolor ','2017-06-02 21:10:03','lok@gmail.com','Onda jedan','pok@gmail.com',0,2,1);
INSERT INTO osa_proba.messages(message_bcc, message_cc, message_content,message_date_time, message_from,message_subject, message_to, message_unread, account_id, folder_id)VALUES(null,null,'Tekst je rt','2016-05-03 20:10:03','thor@gmail.com','Onda broj','david@gmail.com',0,2,1);
INSERT INTO osa_proba.messages(message_bcc, message_cc, message_content,message_date_time, message_from,message_subject, message_to, message_unread, account_id, folder_id)VALUES(null,null,'Tekst je rt','2016-05-03 20:10:03','thor@gmail.com','Onda broj','david@gmail.com',0,2,1);
INSERT INTO osa_proba.messages(message_bcc, message_cc, message_content,message_date_time, message_from,message_subject, message_to, message_unread, account_id, folder_id)VALUES(null,null,'Tekst je jako','2015-05-03 20:10:03','ledda@gmail.com','Pera','pera@gmail.com',0,2,1);
INSERT INTO osa_proba.rules(rule_condition, rule_operation, rule_value, folder_id)VALUES(2, 1, 'vrednost6', 6);
INSERT INTO osa_proba.rules(rule_condition, rule_operation, rule_value, folder_id)VALUES(1, 2, 'vrednost7', 7);
INSERT INTO osa_proba.rules(rule_condition, rule_operation, rule_value, folder_id)VALUES(1, 2, 'vrednost8', 8);
INSERT INTO osa_proba.rules(rule_condition, rule_operation, rule_value, folder_id)VALUES(3, 1, 'vrednost9', 9);
INSERT INTO osa_proba.rules(rule_condition, rule_operation, rule_value, folder_id)VALUES(0, 2, 'vrednost10', 10);
INSERT INTO osa_proba.attachments(attachment_data, attachment_mime_type, attachment_name, message_id)VALUES(null,'cao', 'pera', 7);
INSERT INTO osa_proba.message_tags(message_id, tag_id)VALUES(6, 1);